---
name: Samtök hernaðarandstæðinga
website: https://fridur.is/
scrape:
  source: facebook
  options:
    page_id: Fridarhus
---

SHA er friðarhreyfing sem sameinar stuðningsmenn friðar og afvopnunar. Samtökin berjast gegn hervaldi og ofbeldi.

# Campaign Against Militarism

## About us

Icelanders have opposed military activities and NATO since the time that the country was occupied in 1940, first by the UK and then by the US. One of the demands at a huge demonstration against Iceland’s forcible incorporation into NATO in 1949 was a referendum about Iceland’s entry into NATO.

Iceland’s Campaign Against Militarism has existed in some form or other for a long time, originally being established in Thingvellir National Park in 1960 as “Samtök hernámsandstæðinga” (Campaign Against Military Occupation). That disbanded in 1969 but “Samtök herstöðvaandstæðinga” (Campaign Against Military Bases) was founded in 1972. In 2006 the organization changed its name to “Samtök hernaðarandstæðinga” (Campaign Against Militarism) when the offending US military decided suddenly to physically abandon their base in Iceland (though leaving their toxic waste behind). Environmental effects of military operations, especially those related to operations in Iceland, are the focus of the next issue of the organization’s newsletter/magazine, Dagfari.

Although the military abandoned their Iceland base in September 2006, part of it – the security zone ­– is still closed to the public. CAM has been aware that the US military has requested funds from both the Pentagon and the Icelandic government for upgrading two hangars in the security zone so that two fighter jet squadrons can be located there. The US Navy is now carrying out submarine reconnaissance flights in the GIUK gap – an acronym for Greenland, Iceland and the UK – and also want to provide basic accommodation for up to 1000 military personnel. Therefore CAM has every reason to believe that the US military is thinking about returning to Iceland.

CAM is also attempting to get Iceland to sign the Treaty on the Prohibition of Nuclear Weapons that ICAN initiated and is especially hopeful now as the Prime Minister is from the Left-Green party, which is the only party in power that is against the military and NATO. Left-Green MPs who are members of CAM have proposed that Iceland signs the TPNW.

CAM also sends resolutions to the press about matters such as Donald Trump’s decision to withdraw from the INF treaty. From time to time it also holds meetings on peace-related issues like the situation in Yemen and Syria.

CAM go to speak to NATO soldiers during Trident Juncture

CAM protests when NATO countries engage in military air activities which are held at 3-month intervals and organized a number of activities during the Trident Juncture exercise by NATO in October 2018.

CAM is an active participant in two annual peace events that are organized by a coalition of peace groups. One is a candle-floating ceremony to commemorate the bombing of Hiroshima and Nagasaki, the other is a candle-lit march that takes place early evening on December 23, which in Iceland is a time when many people congregate in the city centre.

In 2005, CAM bought its own premises in central Reykjavik. Besides being a venue for peace-related activities, it is also used by other radical activist and peace groups.
