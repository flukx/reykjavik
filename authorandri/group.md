---
name: Andri Snær Magnason
website: http://www.andrimagnason.com
scrape:
  source: facebook
  options:
    page_id: AndriSnaerMagnason
---
Andri Snær Magnason – author of Tímakistan, Dreamland, LoveStar, The Story of the Blue Planet, Bonus Poetry and more…

Andri Snær Magnason is an Icelandic writer, born in Reykjavik on the 14th of July 1973. His book, On Time and Water was a national best seller in Iceland in 2019 and will be translated to more than 24 languages, coming out in the UK August 2020. His most recent book in english, The Casket of Time, won the Icelandic literary Award and was nominated for the Nordic Council Children’s book Award and has been sold to 14 countries. Andri has written novels, poetry, plays, short stories, essays and CD’s. He is the codirector of the documentary film Dreamland.
