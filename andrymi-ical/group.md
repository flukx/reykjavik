---
name: Andrými (ical)
website: https://www.andrymi.org/
scrape:
  source: ical
  options:
    url: http://andrymi.org/?plugin=all-in-one-event-calendar&controller=ai1ec_exporter_controller&action=export_events&no_html=true
---
Andrými is a combination of two words: “and” and “rými”. The “and” means breathe, spirit, opposition, and “rými” means space. This is the ical version
