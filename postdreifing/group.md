---
name: Post-dreifing
website: https://post-dreifing.is
scrape:
  source: facebook
  options:
    page_id: post.utgafa
---
POST-DREIFING MANIFESTO

# Post-Dreifing Manifesto (íslensku)

post-dreifing er hópur af sjálfstæðu listafólki á stórreykjavíkursvæðinu, sem hefur það að markmiði að auka sýnileika og sjálfbærni í listsköpun í krafti samvinnu.

við afneitum eindregið markaðs- og gróðahyggju í vinnu okkar; við stefnum á að búa til vettvang fyrir sjálfstætt listafólk til að halda áfram að skapa, burtséð frá fjárhagslegri stöðu hvers og eins, og án hvers kyns gróðasjónarmiða.

við afneitum allri stigskiptingu og vinnum alltaf á jöfnum grundvelli innan post-dreifingar.

okkar lýðræði er ekki fulltrúalýðræði, þar sem meirihlutinn ræður, heldur raunverulegt lýðræði, þar sem allir hafa eitthvað að segja. við forðumst tilbrigðaleysi í listsköpun.

DREIFING ER HAFIN.

# Post-Dreifing Manifesto (english)

post-dreifing is an independent arts collective, made up of young artists, based in and around reykjavík. the group’s main goal is to gain visibility and sustainability for artists through collaboration.

we strongly object all intentions to prioritize profit in the creative fields; we aim to create an alternative platform for independent artists to further work on their creative projects, despite the financial position of each artist working towards a state of self-sufficiency, rather than a monetary focus.

We object social and economic hierarchies with our collective creative process and strive to always work on the basis of equality and democratic collaboration.

Our democracy is not a representative democracy where the majority rules, but a true democrazy, where everyone has a say.

We avoid a lack of variability in the collective creative process.

DREIFING ER HAFIN.
