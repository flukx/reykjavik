---
name: Stúdentafélag Háskólans í Reykjavík
website: http://sfhr.is/en/
scrape:
  source: facebook
  options:
    page_id: studentafelag
---
Stúdentafélag Háskólans í Reykjavík (SFHR) is the student association at Reykjavik University. All students, including undergraduate and graduate students are members of SFHR. SFHR was established to form a unity between students and to represent and act in the interests of the students both within and outside the University. SFHR´s board of trustees is composed of three to five members.
