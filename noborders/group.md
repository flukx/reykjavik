---
name: Pepp Íslands
website: https://www.eapn.is
scrape:
  source: facebook
  options:
    page_id: eapn.pepp
---
Pepp er íslenskun á PeP sem stendur fyrir People experiencing Poverty og takmarkið er að vinna gegn fátækt og félagslegri einangrun.
