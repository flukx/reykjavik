---
name: Hugarafl
website: http://www.hugarafl.is/
scrape:
  source: facebook
  options:
    page_id: Hugarafl
---
Hugarafl eru notendastýrð félagasamtök fólks með persónulega reynslu af andlegum áskorunum. ✨Valdefling, batahugmyndafræði og jafningjagrunnur✨

Our principles are personal recovery and healing, empowerment and working together as equals.

Hugarafl (e. Mindpower) is an Icelandic peer run NGO founded in the year 2003 by individuals with a
vast personal and professional knowledge of the mental healthcare system. These individuals had the
common goal of wanting to change the mental healthcare system in Iceland and make it better.
Everything that Hugarafl does is decided upon and done by people with lived experiences of emotional
distress and/or professional background working as equals. Participating in the work of Hugarafl is for
everyone working on their mental health on their own terms.
