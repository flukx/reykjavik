---
name: Reykjavik Tool Library
website: http://www.reykjaviktoollibrary.org/
scrape:
  source: facebook
  options:
    page_id: rvktoollibrary
---
A Tool Library allows members to check out or borrow tools, equipment, functioning as a library would for books, with a membership, which allow members to take tools home for repairs, projects and hobbies. We are a non profit.
