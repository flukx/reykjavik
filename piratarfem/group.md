---
name: Femínistafélag Pírata
website: https://piratar.is/
scrape:
  source: facebook
  options:
    page_id: FemPir
---
Femínistafélags Pírata var stofnað í þeim tilgangi að efla málefnalega umræðu um femínísk málefni innan flokksins svo og útávið.

