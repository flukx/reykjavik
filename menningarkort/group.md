---
name: Menningarkort
website: http://www.menningarkort.is/
scrape:
  source: facebook
  options:
    page_id: menningarkort
---
Menningarkort Reykjavíkur er árskort sem veitir ótakmarkaðan aðgang að söfnum í eigu Reykjavíkurborgar auk margvíslegra fríðinda og sérkjara.
