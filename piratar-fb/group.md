---
name: Píratapartýið
website: https://piratar.is/
scrape:
  source: facebook
  options:
    page_id: PiratarXP
---
Píratar eru stjórnmálaafl sem berst fyrir raunverulegu gegnsæi og ábyrgð í stjórnkerfinu, auknu aðgengi að upplýsingum, beinu lýðræði, upplýsingafrelsi og endurskoðun höfundarréttar.

The Icelandic Pirate Party was founded on November 24th, 2012 based on the political ideology of the Swedish Pirate Party, which Richard Falkvinge founded in January 2006, to bring about internet copyright reform.

There are now Pirate parties in over 60 countries. They each have their own policies and priorities, but all are united in calling for the protection and enhancement of civil rights, including free speech and the right to privacy.
