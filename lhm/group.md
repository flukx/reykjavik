---
name: Landssamtök hjólreiðamanna
website: https://lhm.is/
scrape:
  source: facebook
  options:
    page_id: LandssamtokHjolreidamanna
---
Landssamtök hjólreiðamanna,  LHM,
eru regnhlífasamtök hjólreiðafélaga á Íslandi
sem beita sér fyrir hagsmunum alls hjólreiðafólks.
LHM eiga aðild að Evrópusamtök hjólreiða, European Cyclists' Federation. 
Sjá nánar um markmið LHM, aðildarfélög og önnur hjólafélög hér að neðan.

Our official member organizations include three of the most active cycling clubs in Iceland, The Icelandic mountainbike club, The Cycling Club of Reykjavik and Hjólamenn.
