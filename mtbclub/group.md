---
name: Fjallahjolaklubburinn
website: https://www.fjallahjolaklubburinn.is/
scrape:
  source: facebook
  options:
    page_id: fjallahjolaklubburinn
---
Markmið félagsins er að auka reiðhjólanotkun og vinna að bættri aðstöðu hjólreiðafólks til samgangna og ferðalaga. ÍFHK stendur fyrir útgáfu- og fræðslustarfsemi til að kynna stefnu sína og markmið.
