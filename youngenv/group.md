---
name: Ungir umhverfissinnar
website: http://www.umhverfissinnar.is
scrape:
  source: facebook
  options:
    page_id: umhverfissinnar
---

# Félagið

Ungir umhverfissinnar eru frjáls félagasamtök. Tilgangur félagsins er að vera vettvangur ungs fólks á Íslandi til að láta gott af sér leiða í umhverfismálum. Við viljum hvetja til upplýstrar umræðu um umhverfismál og berjumst fyrir sjálfbærri þróun, náttúruvernd og grænu hagkerfi.

# Starfið

Ungir umhverfissinnar nálgast umhverfismál frá öllum sjónarhornum. Skoðanir okkar byggjast á vísindalegum grunni. Við lítum á fjölbreytni sem styrkleika og erum félag fyrir alla unga umhverfissinna. Við leitumst við að virkja alla félagsmenn til góðra starfa á vegum félagsins. Við höfum jafnrétti og jákvæðni að leiðarljósi og berum virðingu fyrir hvort öðru.

# Grunnhugsjónir

Ungir umhverfissinnar vilja að þörfum núlifandi kynslóðar sé mætt án þess að það komi niður á möguleikum komandi kynslóða og að tilveruréttur náttúrunnar sé viðurkenndur og virtur. Við vinnum í þágu þessarar hugsjónar með því að gera okkar besta til að hafa áhrif á stefnumótun, umræðu og almennt hugarfar í umhverfismálum. Til þess að ná fram markmiðum okkar beitum við stjórnvöld þrýstingi og stuðlum að jafningjafræðslu, viðburðum og útgáfu.

# Málefni

Ungir umhverfissinnar taka afstöðu til málefna á grundvelli grunnhugsjóna sinna. Við leggjum sérstaka áherslu á málefni sem snerta Ísland og íslenskt samfélag. Ungir umhverfissinnar styðja grunnhugmyndir Ríóyfirlýsingarinnar, svo sem varúðarregluna og mengunarbótaregluna. Þeir málaflokkar sem við einbeitum okkur að eru meðal annars loftslagsbreytingar, verndun víðerna, ofneysla og viðhald líffræðilegs og jarðfræðilegs fjölbreytileika.

# Að lokum

Ungir umhverfissinnar vilja lifa í jafnvægi við náttúruna, í samfélagi þar sem maðurinn, framleiðsla hans og neysla eru hluti af náttúrulegri hringrás og þar sem jákvæð umhverfishyggð er jafn sjálfsögð og borgaraleg réttindi.
Samþykkt 4. apríl 2013.

# The Association

The Icelandic Youth Environmentalist Association is a non governmental organisation with the primary objective of giving young people a platform to positively influence the way  society interacts with nature. The association’s goals include promoting and advocating for  holistic environmental governance, sustainable development, nature conservation and a circular economy in accordance with the United Nations Sustainable Development Goals.

# What We Do

The association’s methods are grounded in science and strengthened by a holistic approach. Such holistic methods include  thorough knowledge and understanding of local conditions and community perspectives. Human rights and the rights of nature are the associations guiding lights. Similarly, respect and equality are the cornerstones of the association’s working values. We want our members to be actively  doing good work on behalf of the association.

The Icelandic Youth Environmentalist Association is striving for a world in which the right of nature is acknowledged and respected and where meeting the needs of current generations does not adversely impact future generations.
