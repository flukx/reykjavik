---
name: Andrými
website: https://www.andrymi.org/
scrape:
  source: facebook
  options:
    page_id: andrymi.andrymi
---
Andrými is a combination of two words: “and” and “rými”. The “and” means breathe, spirit, opposition, and “rými” means space.
