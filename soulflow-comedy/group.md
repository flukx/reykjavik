---
name: Soulflow Comedy
website: mailto:soulflow.comedy@gmail.com
scrape:
  source: facebook
  options:
    page_id: soulflow.comedy
---
Soulflow Comedy hosts weekly open mic nights for Women & Queer individuals to have a safe space & platform to practice comedy. We also do private events & parties - get in touch if you wanna book us or sign up for Mondays! ♡♡♡
