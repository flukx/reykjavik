---
name: Druslugangan
website: https://www.druslugangan.is/
scrape:
  source: facebook
  options:
    page_id: Drusluganga
---
Druslugangan eru grasrótarsamtök sem einblína á baráttu gegn kynferðisofbeldi í öllum myndum í öllum kimum samfélagsins. Síðan 2011 hefur druslugangan verið gengin til að minna á að kynferðisofbeldi á sér stað í samfélaginu og því þarf að útrýma.
 
Með Druslugöngunni viljum við færa ábyrgð kynferðisafbrota frá þolendum yfir á gerendur. Druslugangan er gengin hvert ár til að sýna brotaþolum kynferðisofbeldis samstöðu og krefjast betra réttarkerfis og bætts samfélags.

Machine translation:
Druslugangan ist eine Basisorganisation, die sich auf den Kampf gegen sexuelle Gewalt in allen Formen in allen Ecken der Gesellschaft konzentriert. Seit 2011 erinnert der Slut Walk die Menschen daran, dass sexuelle Gewalt in der Gesellschaft stattfindet und daher beseitigt werden muss.
 
Mit dem Slut Walk wollen wir die Verantwortung für Sexualstraftaten von Opfern auf Täter übertragen. Der Slut Walk findet jedes Jahr statt, um Solidarität mit Opfern sexueller Gewalt zu zeigen und ein besseres Rechtssystem und eine bessere Gesellschaft zu fordern.
