---
name: IWW Ísland / Heimssamband verkafólks á Íslandi
website: https://www.iwwisland.org
scrape:
  source: facebook
  options:
    page_id: iwwisland
---
IWW er eitt alþjóðlegt og lýðræðislegt verkalýðsfélag. The IWW is a global democratic union.

*English Below*

Industrial Workers of the World er almennt, alþjóðlegt, róttækt verkalýðsfélag með yfir hundrað ára sögu og tekur nú í fyrsta skipti til starfa á Íslandi.

Róttækt, virkt verkalýðsfélag þar sem meðlimir félagsins eru félagið. Ákvarðanir eru ekki teknar af skriffinnsku bákninu bak við tjöldin heldur beint af félagsmönnum sjálfum. Félagið mun ekki reka sumarbústaði eða veita nokkurskonar óbeina þjónustu heldur sinna raunverulegu hlutverki sínu sem verkalýðsfélag og vinna beint að hagsmunum vinnandi fólks.

# Inngangsorð stjórnarskrár IWW

Verkalýðsstéttin og stétt atvinnurekenda eiga ekkert sameiginlegt. Engin friður mun ríkja svo lengi sem hungur og skortur fyrirfinnst meðal milljóna vinnandi fólks og meðan þau fáu sem tilheyra stétt atvinnurekenda hafa gæði heimsins í höndum sér.

Milli þessara stétta mun ríkja barátta þangað til að verkamenn heimsins skipuleggja sig sem stétt, taka sér yfirráð yfir framleiðsluháttunum, afnema launakerfið og lifa í sátt og samlyndi jörðina.

Við teljum að þar sem að stjórnun atvinnugreina færist í sífellu á færri og færri hendur sé verkalýðsfélögum ófært að sporna gegn sívaxandi valdi atvinnurekenda. Verkalýðsfélögin ala á ástandi sem stillir einum hluta verkamanna upp gegn öðrum í sömu stétt og ónýtir með því launadeilur hvorra annarra. Þar að auki auðvelda verkalýðsfélögin stétt atvinnurekenda að leiða verkamenn í þá villutrú að verkalýðsstéttin eigi sömu hagsmuna að gæta og vinnuveitendur hennar.

Þessu ástandi verður aðeins breytt og hagsmunum hinna vinnandi stétta verður aðeins haldið á lofti af samtökum sem mynduð eru á þann hátt að allir meðlimir þeirra í tiltekinni atvinnugrein, eða öllum atvinnugreinum ef þörf þykir, láti af störfum hvenær sem verkfall eða verkbann er í gangi í einhverri deild samtakanna og geri með því skaða eins skaða allra.

Í stað hins íhaldssama kjörorðs „Sanngjörn dagslaun fyrir sanngjarnt dagsverk” þurfum við að áletra hið byltingarsinnaða vígorð „Afnám launakerfisins” á borða okkar.

Það er sögulegt hlutverk verkalýðsstéttarinnar að losa sig við kapítalismann. Framleiðsluherinn verður að vera skipulagður, ekki aðeins fyrir hversdagslega báráttu við kapítalista, heldur einnig til þess að halda áfram framleiðslunni þegar kapítalismanum hefur verið kollvarpað. Með því að skipleggja á vinnustaðnum erum við að móta byggingu nýs samfélags í skel hins gamla.

*English*

Industrial Workers of the World is a global, international, radical
workers' union that's been going for over a hundred years. We're asking you to join us in opening a branch in Iceland.

http://en.wikipedia.org/wiki/Industrial_Workers_of_the_World

The union is totally run by its rank-and-file members, and its method is direct action. Decisions are not made by a bureaucracy behind the scenes but directly by the members themselves. The union will not operate summer homes or provide social services, but instead focus on what a proper union should do and work directly for the interests of working people.

# Preamble to the IWW Constitution

The working class and the employing class have nothing in common. There can be no peace so long as hunger and want are found among millions of the working people and the few, who make up the employing class, have all the good things of life.

Between these two classes a struggle must go on until the workers of the world organize as a class, take possession of the means of production, abolish the wage system, and live in harmony with the Earth.

We find that the centering of the management of industries into fewer and fewer hands makes the trade unions unable to cope with the ever growing power of the employing class. The trade unions foster a state of affairs which allows one set of workers to be pitted against another set of workers in the same industry, thereby helping defeat one another in wage wars. Moreover, the trade unions aid the employing class to mislead the workers into the belief that the working class have interests in common with their employers.

These conditions can be changed and the interest of the working class upheld only by an organization formed in such a way that all its members in any one industry, or in all industries if necessary, cease work whenever a strike or lockout is on in any department thereof, thus making an injury to one an injury to all.

Instead of the conservative motto, "A fair day's wage for a fair day's work," we must inscribe on our banner the revolutionary watchword, "Abolition of the wage system."

It is the historic mission of the working class to do away with capitalism. The army of production must be organized, not only for everyday struggle with capitalists, but also to carry on production when capitalism shall have been overthrown. By organizing industrially we are forming the structure of the new society within the shell of the old.
