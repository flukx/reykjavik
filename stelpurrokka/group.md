---
name: Stelpur Rokka
website: http://www.stelpurrokka.is/
scrape:
  source: facebook
  options:
    page_id: stelpurrokka
---
Stelpur rokka! (Girls Rock! Iceland) is a volunteer-run non-profit organization working to empower girls, trans boys, gender queer and intersex youth through music. Our core programming focusses on the rock camp.  Campers learn to play an instrument, form bands, and write a song together.  They participate in various workshops on music, gender and social justice, attend lunch time performances by established women musicians and perform live at a final showcase in front of friends and family. 

Over 400 girls and women have participated in our programming over the last 5 years and have formed over 70 bands. At rock camp, campers amplify their already strong voices, strengthen their self-esteem, and collaborate creatively with positive role models. 

Rokksumarbúðir fyrir stelpur, konur, trans og kynsegin einstaklinga
Stelpur rokka! halda rokksumarbúðir fyrir stelpur og trans og kynsegin krakka á Íslandi.

Allar stelpur og trans og kynsegin krakkar á aldrinum 12 - 16 ára eru hvattar til að sækja um og engin hljóðfærakunnátta er nauðsynleg, aðeins áhugi fyrir að læra að rokka og spila í hljómsveit með öðrum.

Hver þátttakandi velur sér hljóðfæri í upphaf námskeiðs að eigin vali, en helstu hljóðfæri í boði eru gítar, bassi, trommur, hljómborð auk söngs. Önnur hljóðfæri eru líka í boði ef óskað er eftir, og stelpur eru velkomnar að koma með sín eigin hljóðfæri.

Í lok sumarbúðanna verða haldnir glæsilegir rokktónleikar þar sem allar hljómsveitirnar koma fram og flytja frumsamin lög.

Innifalið í námskeiðinu er einnig kynningar og vinnusmiðjur á borð við sögu kvenna í rokki, textasmíð, sviðsframkomu og margt fleira.
