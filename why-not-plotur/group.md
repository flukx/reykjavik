---
name: Why not? Plötur
website: https://whynotplotur.bandcamp.com/
scrape:
  source: facebook
  options:
    page_id: whynotplotur
---
Why-not plötur is a self-organised concert venue for the grassroot music scene in reykjavík. they always have free price and provide food at the concerts. 
