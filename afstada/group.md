---
name: Afstaða
website: http://afstada.is
scrape:
  source: facebook
  options:
    page_id: afstada
---
Afstaða, félag fanga og annarra áhugamanna um bætt fangelsismál og betrun.

Markmið félagsins eru fyrst og fremst þau að vinna að tækifæri fyrir fanga til ábyrgðar, endurreisnar og að búa þeim skilyrði til farsællar endurkomu út í samfélag manna.

Hugað verður í hvívetna að aðstandendum fanga, fjölskyldum, vinum og öllum öðrum sem sitja í fangelsi úti í hinu frjálsa samfélagi vegna tengsla við fanga.
